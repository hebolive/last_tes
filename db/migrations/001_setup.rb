Sequel.migration do
  up do
    run <<-SQL
      create extension "uuid-ossp";

      create table users (
        id uuid primary key default uuid_generate_v4() not null,
        name text not null,
        age int,
        company text not null,
        meta jsonb
      );
    SQL
  end

  down do
    # no going back!
  end
end
