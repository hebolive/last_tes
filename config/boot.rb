ENV['RACK_ENV'] ||= 'development'

require 'bundler'
require 'active_support/all'

Bundler.require(:default, ENV['RACK_ENV'])

