# This file defines methods used for connecting to a database. It is unlikely
# you need to change anything here.

# This should be greater than the number of puma threads used
ENV['DB_MAX_POOL_SIZE'] ||= '10'

def database_name
  case ENV['RACK_ENV']
  when 'development'
    'interview-development'
  when 'test'
    'interview-test'
  else
    raise "Can not create DB in #{ENV['RACK_ENV']} environment."
  end
end

def database_url
  ENV['DATABASE_URL'] || "postgres://localhost/#{database_name}"
end

def database_connect
  Sequel.connect(database_url, max_connections: ENV['DB_MAX_POOL_SIZE'])
  Sequel::Model.db.extension(:pagination)
end
