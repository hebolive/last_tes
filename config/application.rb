app_path = File.expand_path('../app', __FILE__)
$LOAD_PATH.unshift(app_path)

require_relative 'boot'
require_relative 'database'
require_relative 'initializers/sequel'
# Initialize db connection
DB = database_connect

# dependencies here (in order required)
require_relative '../app/models/user'
require_relative '../app/models/organization'

require_relative '../app/actions'

