# Gives us things like 'validates_presence'
Sequel::Model.plugin :validation_helpers

# Don't raise SequelExceptions if validations fail. Still raises if there are
# PostgreSQL constraint issues or other PostgreSQL eceptions
Sequel::Model.raise_on_save_failure = false

# All /datetimes/ in the DB are UTC.
Sequel.default_timezone = :utc

# Adds a couple of methods that make it easier to deal with objects
# which may or may not yet exist in the database
Sequel::Model.plugin :update_or_create
