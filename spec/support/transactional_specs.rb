RSpec.configure do |config|
  # Delete everything to ensure that our database is empty before running any
  # examples.
  config.before(:all) do
    tables_to_truncate = DB.tables - [:schema_info]
    tables_to_truncate.each do |table|
      DB[table].truncate(cascade: true)
    end
  end

  # Encapsulate each example in its own transaction, which we can roll back
  # before the next example starts.
  config.around(:each) do |example|
    DB.transaction(rollback: :always, auto_savepoint: true) { example.run }
  end
end

