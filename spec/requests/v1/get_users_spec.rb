require File.expand_path '../../../spec_helper.rb', __FILE__

RSpec.describe 'GET /v1/users', type: :request do
  subject { get('/v1/users') }

  context 'when there are no users' do
    it 'should be successful' do
      subject
      expect(response_status).to eq(200)
    end

    it 'should respond with empty json' do
      subject
      expect(response_json('users')).to eq([])
    end
  end

  context 'when there are users' do
    let!(:user) do
      User.create(
        name: 'John Carmack',
        age: 45,
        company: 'iD Software'
      )
    end

    it 'should be successful' do
      subject
      expect(response_status).to eq(200)
    end

    it 'should include the user' do
      subject
      expect(response_json('users')).to include(user.as_json)
    end
  end

  context 'when there is more than 10 in database, it should page' do
    let!(:users) do
      1..21.times do |n|
        User.create(
          name: "John Carmack #{n}",
          age: 45,
          company: 'iD Software'
        )
      end
    end

    it 'should bring 10 when no page defined' do
      get('/v1/users')
      expect(response_json('users').size).to eq(10)
    end

    it 'should bring 10 first page' do
      get('/v1/users?page=1')
      expect(response_json('users').size).to eq(10)
    end

    it 'should bring 10 second page' do
      get('/v1/users?page=2')
      expect(response_json('users').size).to eq(10)
    end

    it 'should bring 1 third page' do
      get('/v1/users?page=3')
      expect(response_json('users').size).to eq(1)
    end

    it 'should bring 0 fourth page' do
      get('/v1/users?page=4')
      expect(response_json('users').size).to eq(0)
    end
  end
end
