require File.expand_path '../../../spec_helper.rb', __FILE__

RSpec.describe 'DELETE /v1/user/:id', type: :request do
  subject { delete("/v1/user/#{user.id}") }

  context 'when the user does not exist' do
    let(:user) { double(id: SecureRandom.uuid) }

    it 'should be not found' do
      subject
      expect(response_status).to eq(404)
    end

    it 'should have not found message' do
      subject
      expect(response_json('message')).to eq('user not found')
    end
  end

  context 'when user does exist' do
    let!(:user) do
      User.create(
        name: 'John Carmack',
        age: 45,
        company: 'iD Software'
      )
    end

    it 'should be successful' do
      subject
      expect(response_status).to eq(200)
    end

    it 'should include the user' do
      subject
      expect(response_json('message')).to eq('user was deleted')
    end
  end
end
