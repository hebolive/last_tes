require 'sequel/extensions/pagination'
module Actions
  extend ActiveSupport::Concern

  included do
    get '/v1/user/:id', {provides: 'json'} do
      user = User.find(id: params[:id])

      if user
        [200,  { user: user.as_json }]
      else
        [404, {message: 'user not found' } ]
      end
    end

    get '/v1/users', provides: :json do
      page = params[:page] || 1
      users = User.dataset.paginate(page.to_i, 10)

      [200, { users: users.as_json }]
    end

    delete '/v1/user/:id', provides: :json do
      user = User.find(id: params[:id])

      if user
        if user.delete
          [200, { message: 'user was deleted' }]
        else
          [422, {message: 'error deleting user'}]
        end
      else
        [404, {message: 'user not found'} ]
      end
    end
  end
end
