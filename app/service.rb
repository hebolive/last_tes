require_relative '../config/application'

class Service < Sinatra::Base
  use Rack::Parser
  include Actions

  after do
    body = response.body
    response.body = JSON.dump(body) if body.is_a?(Hash)
  end
end
