# Shore Ruby Interview Challenge

Hi there! Welcome to the Shore interview. Your first task is to add a feature to
this 'user management' project. But oh wait! It looks like an intern started the
project before they went back to school! Please fix their mistakes and implement
your feature.

##### Requirements

1. postgres 9.4 is installed with the 'uuid-ossp' extension.
2. ruby 2.3.1 is installed.

##### Setup

```
bundle install
RACK_ENV=test bundle exec rake db:setup
bundle exec rspec
```

##### Tasks
1) Fix any bugs left behind by the previous developer.
2) Add pagination to the users table with tests.
3) if you have time - add token auth to the project.

